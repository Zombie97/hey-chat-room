//Require the express moule
const express = require('express');

//create a new express application
const app = express()

//require the http module
const http = require('http').Server(app)

// require the socket.io module
const io = require('socket.io')(http, {
    cors: {
        origin: "https://hey-chat-room.web.app",
        methods: ["GET", "POST"],
        credentials: true,
    },
    allowEIO3: true,
    transport: ['websocket', 'polling']
});

const port = 8080;

const bodyParser = require("body-parser");
const chatRouter = require("./route/chatroute");
const userRouter = require("./route/userRoute");
const chatRoomRouter = require("./route/chatRoomRoute");
const cors = require('cors');

const allowedOrigins = [
    'https://hey-chat-room.web.app',
    'http://localhost:3000'

];



// Reflect the origin if it's in the allowed list or not defined (cURL, Postman, etc.)
const corsOptions = {
    origin: (origin, callback) => {
        if (allowedOrigins.includes(origin) || !origin) {
            callback(null, true);
        } else {
            callback(new Error('Origin not allowed by CORS'));
        }
    }
}

// Enable preflight requests for all routes
// app.options('*', cors(corsOptions));

app.use(cors(corsOptions));

//bodyparser middleware
app.use(bodyParser.json());

//routes
app.use("/chats", chatRouter);
app.use("/users", userRouter);
app.use("/room", chatRoomRouter);


/** catch 404 and forward to error handler */
app.use('*', (req, res) => {
    return res.status(404).json({
        success: false,
        message: 'API endpoint doesnt exist'
    })
});


//setup event listener
io.on('connection', (socket) => {
    console.log('initConnect');
    socket.on('connection', socket => {
        console.log("user connected");
        socket.on("disconnect", function() {
            console.log("user disconnected");
        });

    });
    socket.on('chat-message', (msg) => {
        console.log('message in back: ', msg);

        io.emit('received', msg)
    });
    socket.on('new-user', (msg) => {
        console.log('user in back: ', msg);

        io.emit('user', msg)
    });
});



//wire up the server to listen to our port 3000
http.listen(port, () => {
    console.log('connected to port: ' + port)
});