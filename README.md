# Challenge Full Stack Developer (Chat Room)


## Especificaciones

- #### Para probar la aplicación siga la url [https://hey-chat-room.web.app] [app]
- #### El servidor se encuentra funcionando en línea en la url [https://hey-chat-room.uc.r.appspot.com] [server]
- #### La base de datos se encuentra alojada en la ip publica [http://34.133.150.29:27017] [db]





## Instalar localmente 



El proyecto se divide en dos partes la vista web creada con Vue cli y el el servidor creado con nodeJS y Express.

> Nota: Para la correcta funcionalidad de socket.io se recomienda probar la aplicación en producción



clonar el repositorio

```sh
git clone https://bitbucket.org/Zombie97/hey-chat-room/src
```
Para arrancar de forma local el servidor se requiere tener instalado NODEJS


```sh
cd hey-chat-rom
npm install 
npm run serve
```
El servidor se ejecutara en http://localhost:8080

Para ejecutar el front-end  de forma local 


```sh
cd hey-chat-rom/front-end
npm install 
npm run build
npm run serve
```
El front-end se ejecutara en http://localhost:5000 
> Nota:  El puerto puede cambiar, al ejecutar `npm run dev`  en la terminal aparece la url.


## Plugins
Las herramientas usadas en el servidor.

| Plugin  |
| ------  |
|[@withvoid/make-validation] [AXIOS] |
| [body-parser] [socket.io-client] |
| [cors] [vue-socket.io-extended] |
| [express] [vue3-form-validation] |
| [mongoose] [vue-router] |
| [socket.io] [tailwind] |
| [uuid] [uuid] |


Las herramientas usadas en el front-end.

| Plugin  |
| ------  |
 [axios][AXIOS] |
| [socket.io-client] [socket.io-client] |
| [vue-socket.io-extended] [vue-socket.io-extended] |
| [vue3-form-validation] [vue3-form-validation] |
| [vue-router] [vue-router] |
| [tailwind] [tailwind] |
| [vite] [vite] |



#### Compilar para producción

Para compilar el servidor se requiere tener instalado el [sdk de google] [gcloud]

```sh
cd hey-chat-rom
gcloud app deploy
```

para compilar el front-end 

```sh
cd hey-chat-rom/front-end
npm run deploy
```

## Mongo DB

Como Base de datos se uso MongoDB, alojada en Google Cloud 

```sh
IP publica 34.133.150.29:27017
Nombre de la base de datos chat-room
```
## Hostings
Para el hosting del front end de la aplicación se usa [Firebase Hosting][firebaseh].
El servidor se encuentra alojado en google cloud con [App Engine][appeng].
La base de datos se ecuentra alojada en una instancia VM en Google Cloud.


## License

MIT

**Free Software, Hell Yeah!**

 
   [node.js]: <http://nodejs.org>
   [express]: <http://expressjs.com>
   [AXIOS]: <https://github.com/axios/axios>
   [socket.io-client]: <https://github.com/socketio/socket.io-client#readme>
   [vue-socket.io-extended]: <https://github.com/probil/vue-socket.io-extended#readme>
   [vue3-form-validation]: <https://github.com/JensDll/vue3-form-validation#readme>
   [vue-router]: <https://github.com/vuejs/vue-router>
   [tailwind]: <https://github.com/tailwindlabs/tailwindcss>
   [@withvoid/make-validation]: <https://github.com/withvoid/make-validation>
   [body-parser]: <https://github.com/expressjs/body-parser>
   [cors]: <https://github.com/expressjs/cors>
   [express]: <express>
   [mongoose]: <https://github.com/Automattic/mongoose>
   [uuid]: <https://github.com/uuidjs/uuid>
   [vite]: <https://github.com/vitejs/vite>
  [gcloud]: <https://cloud.google.com/sdk/docs/install>
  [firebaseh]: <https://firebase.google.com/docs/hosting>
   [app]: <https://hey-chat-room.web.app>
   [server]: <https://hey-chat-room.uc.r.appspot.com>
   [db]: <http://34.133.150.29:27017>
  