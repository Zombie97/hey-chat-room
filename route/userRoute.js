const express = require("express");

const router = express.Router();
const userController = require('../controllers/userController');
const User = new userController()
router
    .get('/', User.onGetAllUsers)
    .post('/', User.onCreateUser)
    .get('/:id', User.onGetUserById)
    .delete('/:id', User.onDeleteUserById)


module.exports = router;