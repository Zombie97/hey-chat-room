const express = require("express");
const router = express.Router();

const chatController = require('../controllers/chatController');
const Chat = new chatController()
router
    .get('/', Chat.onGetAllMessages)
    .post('/', Chat.onCreateMessage)
    .get('/:id', Chat.onGetMessageByID)

module.exports = router;