const express = require("express");
const router = express.Router();

const chatRoomController = require('../controllers/chatRoomController');
const ChatRoom = new chatRoomController()
router
    .get('/', ChatRoom.getRecentConversation)
    .get('/:roomId', ChatRoom.getConversationByRoomId)
    .post('/initiate', ChatRoom.initiate)
    .post('/:roomId/message', ChatRoom.postMessage)
    .put('/:roomId/mark-read', ChatRoom.markConversationReadByRoomId)

module.exports = router;