const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const { v4 } = require("uuid");
const uuidv4 = v4
const userSchema = new Schema({
    _id: {
        type: String,
        default: () => uuidv4().replace(/\-/g, ""),
    },
    userName: {
        type: String
    }
}, {
    timestamps: true,
    collection: "users"
});

/**
 * @param {Array} ids, string of user ids
 * @return {Array of Objects} users list
 */
userSchema.statics.getUserByIds = async function(ids) {
    try {
        const users = await this.find({ _id: { $in: ids } });
        return users;
    } catch (error) {
        throw error;
    }
}

/**
 * @param {String} id - id of user
 * @return {Object} - details of action performed
 */
userSchema.statics.deleteByUserById = async function(id) {
    try {
        const result = await this.remove({ _id: id });
        return result;
    } catch (error) {
        throw error;
    }
}


let UserModel = mongoose.model("User", userSchema);
module.exports = UserModel;