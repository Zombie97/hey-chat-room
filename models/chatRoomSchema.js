const mongoose = require("mongoose");
const { v4 } = require("uuid");
const uuidv4 = v4

const chatRoomSchema = new mongoose.Schema({
    _id: {
        type: String,
        default: () => uuidv4().replace(/\-/g, ""),
    },
    members: Array,
    chatInitiator: String,
}, {
    timestamps: true,
    collection: "chatrooms",
});

/**
 * @param {String} userId - id of user
 * @return {Array} array of all chatroom that the user belongs to
 */
chatRoomSchema.statics.getChatRoomsByUserId = async function(userId) {
    try {
        const rooms = await this.find({ members: { $all: [userId] } });
        return rooms;
    } catch (error) {
        throw error;
    }
}

/**
 * @param {String} roomId - id of chatroom
 * @return {Object} chatroom
 */
chatRoomSchema.statics.getChatRoomByRoomId = async function(roomId) {
    try {
        const room = await this.findOne({ _id: roomId });
        return room;
    } catch (error) {
        throw error;
    }
}

/**
 * @param {Array} members - array of strings of participants in the chat
 * @param {String} chatInitiator - user who initiated the chat
 */
chatRoomSchema.statics.initiateChat = async function(members, chatInitiator) {
    try {
        const availableRoom = await this.findOne({
            members: {
                $size: members.length,
                $all: [...members],
            }
        });
        if (availableRoom) {
            return {
                isNew: false,
                message: 'retrieving an old chat room',
                chatRoomId: availableRoom._doc._id,
            };
        }

        const newRoom = await this.create({ members, chatInitiator });
        return {
            isNew: true,
            message: 'creating a new chatroom',
            chatRoomId: newRoom._doc._id,
        };
    } catch (error) {
        console.log('error on start chat method', error);
        throw error;
    }
}


let ChatRoomModel = mongoose.model("ChatRoom", chatRoomSchema);
module.exports = ChatRoomModel;