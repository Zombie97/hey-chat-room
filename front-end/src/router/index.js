import { createRouter, createWebHistory } from 'vue-router';
import auth from '../modules/auth'

const routerHistory = createWebHistory()
    // import { computed } from "vue";
const authGuard = (to, from, next) => {
    console.log('set user init auth guard-> auth.getters', auth.getters.isAuthenticated, to)

    if (auth.getters.isAuthenticated) {


        if (to.path === "/register") {
            next('/chat')
        } else if (auth.getters.isAuthenticated) {
            next();
        }

    } else {
        // No user is signed in.
        console.log('auth.getters.isAuthenticated', auth.getters.isAuthenticated, 'enter to /login')
        next("/login")
    }

};
const routes = [{
        path: '/',
        component: import ( /* webpackChunkName: "chat" */ '../views/member/chat.vue'),
        beforeEnter: authGuard
    },
    {
        path: '/login',
        name: 'login',
        component: () =>
            import ( /* webpackChunkName: "login" */ '../views/public/login.vue'),

    },
    {
        path: '/register',
        name: 'register',
        component: () =>
            import ( /* webpackChunkName: "register" */ '../views/public/register.vue'),

    },

    {
        path: '/chat',
        name: 'chat',
        component: () =>
            import ( /* webpackChunkName: "chat" */ '../views/member/chat.vue'),
        beforeEnter: authGuard
    },

]

const router = createRouter({
    history: routerHistory,
    routes,
    //base: '/',
    // root: '/'
})


export default router