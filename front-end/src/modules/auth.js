import { createStore } from 'vuex'
import { dbService } from '../modules/dbService'
export default createStore({
    state: {
        userData: {},
        isBusy: false,
        error: "",
        isAuthenticated: false,

    },
    mutations: {
        setBusy: (state) => state.isBusy = true,
        clearBusy: (state) => state.isBusy = false,
        setError: (state, error) => state.error = error,
        clearError: (state) => state.error = "",
        setUser: (state, model) => {
            console.log('set user init store', state, 'model: ', model)
            state.userData = Object.assign(state.userData, model);
            state.isAuthenticated = true;
        },
        clearToken: (state) => {
            state.userData = "";
        }
    },
    getters: {
        isAuthenticated: (state) => { console.log('estate', state.isAuthenticated); return state.isAuthenticated },
        userData: (state) => { console.log('userData', state.userData); return state.userData }
    },
    actions: {
        login: async({ commit }, model) => {
            return new Promise(async(resolve, reject) => {
                try {
                    commit("setBusy");
                    commit("clearError");
                    const loginUser = await new dbService().loginUser(model);
                    if (loginUser.success) {
                        commit("setUser", loginUser.user);
                        commit("clearBusy");
                        resolve(true)
                        return true
                    } else {
                        commit("clearBusy");
                        commit("setError", "Failed register");
                        reject(false);
                        return false;
                    }
                } catch (e) {
                    console.error(e);
                    commit("setError", "Failed to login");
                    reject(e)
                }
            });
        },
        logout: async({ commit }) => {
            commit("clearToken");
            await auth.signOut();
            await router.push("/");
        },
        registerUser: async({ commit }, model) => {
            return new Promise(async(resolve, reject) => {
                try {
                    commit("setBusy");
                    commit("clearError");
                    const newUser = await new dbService().registerUser(model);
                    if (newUser.success) {
                        commit("setUser", newUser.user);
                        commit("clearBusy");
                        resolve(newUser.user)
                        return true
                    } else {
                        commit("clearBusy");
                        commit("setError", "Failed register");
                        reject(e)
                        return false;
                    }
                } catch (e) {
                    console.error(e);
                    commit("setError", "Failed to login");
                    reject(e)
                }
            });
        }

    },
})