import axios from 'axios'
export class dbService {

    async getAllUsers() {
        try {
            const res = await axios.get(`https://hey-chat-room.uc.r.appspot.com/users`);
            return res.data;
        } catch (e) {
            console.error('error at getting users', e)
            return e
        }
    }
    async loginUser(userModel) {
        try {
            const res = await axios.get(`https://hey-chat-room.uc.r.appspot.com/users/${userModel.userName}`);
            return res.data;
        } catch (e) {

            return e
        }
    }
    async registerUser(userModel) {
        try {
            const res = await axios.post(`https://hey-chat-room.uc.r.appspot.com/users`, {

                userName: userModel.userName
            });

            return res.data;
        } catch (e) {
            console.error('error at getting users', e)
            return e
        }
    }
    async initiateChat(currentUser, chatPartner) {
        try {
            const res = await axios.post(`https://hey-chat-room.uc.r.appspot.com/room/initiate`, {

                members: [currentUser._id, chatPartner._id],
                chatInitiator: currentUser._id
            });

            return res.data;
        } catch (e) {
            console.error('error at getting users', e)
            return e
        }
    }
    async getConversation(room) {
        try {
            const res = await axios.get(`https://hey-chat-room.uc.r.appspot.com/room/${room.chatRoomId}`);
            return res.data;
        } catch (e) {
            console.error('error at getting users', e)
            return e
        }
    }
    async postMessage(room, message, currentUser) {
        try {
            const res = await axios.post(`https://hey-chat-room.uc.r.appspot.com/room/${room.chatRoomId}/message`, {

                roomId: room.chatRoomId,
                messageText: message,
                userId: currentUser._id
            });
            return res.data;
        } catch (e) {
            console.error('error at getting users', e)
            return e
        }
    }
}