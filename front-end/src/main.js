import { createApp } from 'vue'
import router from './router'
import App from './App.vue'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import './assets/stylesheets/main.scss';
import VueSocketIOExt from "vue-socket.io-extended";
import io from "socket.io-client";
const socket = io('https://hey-chat-room.uc.r.appspot.com', {
    cors: ['*'],
    allowEIO3: true
});
const app = createApp(App);
app.use(router);
app.use(VueSocketIOExt, socket);
app.use(VueSweetalert2);
app.mount('#app')


/*
.use(new VueSocketIO({
    debug: true,
    connection: 'https://hey-chat-room.uc.r.appspot.com',
    extraHeaders: {
        'Access-Control-Allow-Credentials': true
    },
    allowEIO3: true
}))*/