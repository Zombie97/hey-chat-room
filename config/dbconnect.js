const mongoose = require("mongoose");
mongoose.Promise = require("bluebird");
const { databaseConfig } = require('./credentials.js');
const CONNECTION_URL = `mongodb://${databaseConfig.URLServer}/${databaseConfig.dataBaseName}`

const connect = mongoose.connect(CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true });

mongoose.connection.on('connected', () => {
    console.log('Mongo has connected succesfully')
});
mongoose.connection.on('reconnected', () => {
    console.log('Mongo has reconnected')
});
mongoose.connection.on('error', error => {
    console.log('Mongo connection has an error', error)
    mongoose.disconnect()
});
mongoose.connection.on('disconnected', () => {
    console.log('Mongo connection is disconnected')
});
module.exports = connect;