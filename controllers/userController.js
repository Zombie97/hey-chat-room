const UserModel = require("./../models/userSchema");
const connectdb = require("./../config/dbconnect");
const makeValidation = require('@withvoid/make-validation');


class userController {
    async onGetAllUsers(req, res) {
        try {
            await connectdb;
            const Users = await UserModel.find({});
            return res.status(200).json({ success: true, Users });
        } catch (error) {
            return res.status(500).json({ success: false, error: error })
        }
    }
    async onGetUserById(req, res) {
        try {

            await connectdb;
            console.log(req.params.id);
            const user = await UserModel.findOne({ userName: req.params.id });
            if (!user) throw ({ error: 'No user with this id found' });
            return res.status(200).json({ success: true, user });
        } catch (error) {
            return res.status(500).json({ success: false, error: error })
        }
    }
    async onCreateUser(req, res) {
        try {
            const validation = makeValidation(types => ({
                payload: req.body,
                checks: {
                    userName: { type: types.string },

                }
            }));
            if (!validation.success) return res.status(400).json({...validation });
            const { userName } = req.body;
            const exist = await UserModel.findOne({ userName: req.body.userName });
            if (exist) throw ({ error: 'No user with this id found' });
            const user = await UserModel.create({ userName });
            return res.status(200).json({ success: true, user });
        } catch (error) {
            console.log(error)
            return res.status(500).json({ success: false, error: error })
        }
    }

    async onDeleteUserById(req, res) {
        try {
            const user = await UserModel.deleteByUserById(req.params.id);
            return res.status(200).json({
                success: true,
                message: `Deleted a count of ${user.deletedCount} user.`
            });
        } catch (error) {
            return res.status(500).json({ success: false, error: error })
        }
    }

}
module.exports = userController;