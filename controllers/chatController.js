const ChatModel = require("../models/chatMessageSchema");
const connectdb = require("./../config/dbconnect");
const makeValidation = require('@withvoid/make-validation');

class userController {
    async onGetAllMessages(req, res) {
        try {
            await connectdb;
            const Users = await ChatModel.find({});
            return res.status(200).json({ success: true, Users });
        } catch (error) {
            console.log(error);
            return res.status(500).json({ success: false, error: error })
        }
    }
    async onGetMessageByID(req, res) {
        try {

            await connectdb();
            const user = await ChatModel.findById(req.params.id);
            return res.status(200).json({ success: true, user });
        } catch (error) {
            return res.status(500).json({ success: false, error: error })
        }
    }
    async onCreateMessage(req, res) {
        try {
            const validation = makeValidation(types => ({
                payload: req.body,
                checks: {
                    sender: { type: types.string },
                    message: { type: types.string },

                }
            }));
            if (!validation.success) return res.status(400).json({...validation });

            const { sender, message } = req.body;
            const chat = await ChatModel.create(sender, message);
            return res.status(200).json({ success: true, chat });
        } catch (error) {
            return res.status(500).json({ success: false, error: error })
        }
    }

}
module.exports = userController;