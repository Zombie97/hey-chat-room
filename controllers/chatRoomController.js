const ChatRoomModel = require("./../models/chatRoomSchema");
const ChatMessageModel = require("./../models/chatMessageSchema");
const connectdb = require("./../config/dbconnect");
const makeValidation = require('@withvoid/make-validation');
const UserModel = require("./../models/userSchema");
class chatRoomController {
    async initiate(req, res) {
        try {
            console.log('init');
            await connectdb;
            const validation = makeValidation(types => ({
                payload: req.body,
                checks: {
                    members: {
                        type: types.array,
                        options: { unique: true, empty: false, stringOnly: true }
                    },
                }
            }));
            if (!validation.success) return res.status(400).json({...validation });

            const { members } = req.body;
            const { chatInitiator } = req.body;
            console.log({ members });
            console.log({ chatInitiator });
            const allmembers = [...members, chatInitiator];
            const chatRoom = await ChatRoomModel.initiateChat(allmembers, chatInitiator);
            return res.status(200).json({ success: true, chatRoom });
        } catch (error) {
            console.error('error at initate chat', error);
            return res.status(500).json({ success: false, error: error })
        }
    }
    async postMessage(req, res) {
        try {
            await connectdb;
            const { roomId } = req.params;
            const validation = makeValidation(types => ({
                payload: req.body,
                checks: {
                    messageText: { type: types.string },
                }
            }));
            if (!validation.success) return res.status(400).json({...validation });

            const messagePayload = {
                messageText: req.body.messageText,
            };
            const currentLoggedUser = req.body.userId;
            console.log(roomId, messagePayload, currentLoggedUser);
            const post = await ChatMessageModel.createPostInChatRoom(roomId, messagePayload, currentLoggedUser);
            // global.io.sockets.in(roomId).emit('new message', { message: post });
            return res.status(200).json({ success: true, post });
        } catch (error) {
            console.error('error at postMessage', error);
            return res.status(500).json({ success: false, error: error })
        }
    }
    async getRecentConversation(req, res) {
        try {
            await connectdb;
            const currentLoggedUser = req.userId;
            const options = {
                page: parseInt(req.query.page) || 0,
                limit: parseInt(req.query.limit) || 10,
            };
            const rooms = await ChatRoomModel.getChatRoomsByUserId(currentLoggedUser);
            const roomIds = rooms.map(room => room._id);
            const recentConversation = await ChatMessageModel.getRecentConversation(
                roomIds, options, currentLoggedUser
            );
            return res.status(200).json({ success: true, conversation: recentConversation });
        } catch (error) {
            return res.status(500).json({ success: false, error: error })
        }
    }
    async getConversationByRoomId(req, res) {
        try {
            await connectdb;
            const { roomId } = req.params;

            const room = await ChatRoomModel.getChatRoomByRoomId(roomId)
            if (!room) {
                return res.status(400).json({
                    success: false,
                    message: 'No room exists for this id',
                })
            }
            console.log(room)
            const users = await UserModel.getUserByIds(room.members);
            console.log('users', users)
            const options = {
                page: parseInt(req.query.page) || 0,
                limit: parseInt(req.query.limit) || 10,
            };
            const conversation = await ChatMessageModel.getConversationByRoomId(roomId, options);
            return res.status(200).json({
                success: true,
                conversation,
                users,
            });
        } catch (error) {
            console.error('error at getConversationByRoomId', error);
            return res.status(500).json({ success: false, error });
        }
    }
    async markConversationReadByRoomId(req, res) {
        try {
            await connectdb;
            const { roomId } = req.params;
            const room = await ChatRoomModel.getChatRoomByRoomId(roomId)
            if (!room) {
                return res.status(400).json({
                    success: false,
                    message: 'No room exists for this id',
                })
            }

            const currentLoggedUser = req.userId;
            const result = await ChatMessageModel.markMessageRead(roomId, currentLoggedUser);
            return res.status(200).json({ success: true, data: result });
        } catch (error) {
            console.log(error);
            return res.status(500).json({ success: false, error });
        }
    }
}
module.exports = chatRoomController;